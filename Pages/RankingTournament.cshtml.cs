﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ClubWebsite.Pages
{
    public class RankingTournamentModel : PageModel
    {
        public string Message { get; set; }

        public void OnGet()
        {
            Message = "Longhorn Invitational Ranking Tournament";
        }
    }
}
